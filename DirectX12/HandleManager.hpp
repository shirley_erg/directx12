#ifndef __HANDLEMANAGER_HPP__
#define __HANDLEMANAGER_HPP__
#include <Windows.h>

class HandleManager {
private:
	HANDLE handle;
public:
	HandleManager() = default;
	HandleManager(HANDLE&& ManagementHandle);
	~HandleManager();
	HandleManager(const HandleManager&) = delete;
	HandleManager(HandleManager&& h);
	HandleManager& operator = (const HandleManager&) = delete;
	HandleManager& operator = (HandleManager&& h) noexcept;
	HANDLE* operator & () noexcept;
	HANDLE Get() const noexcept;
	HANDLE* GetAddressOf() noexcept;
	HANDLE& GetReference() noexcept;
	const HANDLE& GetReference() const noexcept;
	bool operator ! () const noexcept;
};
#endif
