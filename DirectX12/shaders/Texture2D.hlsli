cbuffer cbTansMatric : register(b0) {
	float4x4 WVP;
};

Texture2D<float4> tex : register(t0);
SamplerState samp0 : register(s0);

struct VS_Input {
	float3 Position : POSITION;
	float3 Normal : NORMAL;
	float2 UV : TEXCOORD;
};

struct PS_Input {
	float4 Position : SV_POSITION;
	float4 Normal : NORMAL;
	float2 UV : TEXCOORD;
};
