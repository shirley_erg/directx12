#include "Texture2D.hlsli"

float4 main(PS_Input input) : SV_TARGET {
	return tex.Sample(samp0, input.UV);
}
