#include "Texture2D.hlsli"

PS_Input main(VS_Input input) {
	PS_Input output;
	float4 Pos = float4(input.Position, 1.0f);
	float4 Nrm = float4(input.Normal, 1.0f);
	output.Position = mul(Pos, WVP);
	output.Normal = mul(Nrm, WVP);
	output.UV = input.UV;
	return output;
}
