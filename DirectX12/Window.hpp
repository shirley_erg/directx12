#ifndef __WINDOW_HPP__
#define __WINDOW_HPP__
#include <Windows.h>
#include <string>
#include <array>

class Color {
private:
	int Red, Green, Blue;
public:
	Color() = default;
	Color(const int Red, const int Green, const int Blue)
		: Red(Red), Green(Green), Blue(Blue) {}
	Color(const std::string ColorCode);
	Color(const std::wstring ColorCode);
	HBRUSH GetColor() const noexcept;
	std::array<float, 4> operator [] (const float Alpha) const noexcept;
};

enum class WindowControlButton : UINT {
	Minimize = SC_MINIMIZE,
	Maximize = SC_MAXIMIZE,
	Close = SC_CLOSE
};

class ImageIcon {
private:
	std::string FilePath;
	int IconWidth, IconHeight;
public:
	ImageIcon() = default;
	ImageIcon(const std::string IconFilePath, const int Width, const int Height)
		: FilePath(IconFilePath), IconWidth(Width), IconHeight(Height) {}
	HICON GetImage(HINSTANCE InstanceHandle, const unsigned int LoadOption, const bool Cursor = false) const noexcept;
	bool empty() const { return this->FilePath.empty(); }
};

class MainWindow {
private:
	WNDCLASSEXA wc;
	std::string WindowTitle;
	HINSTANCE InstanceHandle;
	MainWindow(HINSTANCE hInstance, WNDPROC WndProc, LPCSTR MenuName, const std::string WindowTitle, const std::string BackgroundColorCode,
		const HICON Icon, const HICON Cursor);
public:
	MainWindow() = default;
	MainWindow(HINSTANCE hInstance, WNDPROC WndProc, LPCSTR MenuName, const std::string WindowTitle, const std::string BackgroundColorCode);
	MainWindow(HINSTANCE hInstance, WNDPROC WndProc, LPCSTR MenuName, const std::string WindowTitle, const std::string BackgroundColorCode,
		const ImageIcon Icon, const ImageIcon Cursor);
	void CreateClientWindow(const int WindowWidth, const int WindowHeight);
	void Show(const int CmdShow);
	void Update();
	HWND hWnd;
	int WindowWidth, WindowHeight;
};
#endif
