// HandleManager.cpp : DLL アプリケーション用にエクスポートされる関数を定義します。
//

#include "HandleManager.hpp"
#include <utility>

HandleManager::HandleManager(HANDLE&& ManagementHandle) : handle(std::move(ManagementHandle)) {
	if (ManagementHandle != nullptr) ManagementHandle = nullptr;
}

HandleManager::~HandleManager() { CloseHandle(this->handle); }

HandleManager::HandleManager(HandleManager&& h) : handle(std::move(h.handle)) {}

HandleManager& HandleManager::operator = (HandleManager&& h) noexcept {
	this->handle = std::move(h.handle);
	h.handle = nullptr;
	return *this;
}

HANDLE* HandleManager::operator & () noexcept { return this->GetAddressOf(); }

HANDLE HandleManager::Get() const noexcept { return this->handle; }

HANDLE* HandleManager::GetAddressOf() noexcept { return &this->handle; }

HANDLE& HandleManager::GetReference() noexcept { return this->handle; }

const HANDLE& HandleManager::GetReference() const noexcept { return this->handle; }

bool HandleManager::operator ! () const noexcept { return this->handle == NULL; }
