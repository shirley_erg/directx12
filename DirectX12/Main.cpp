#include "Window.hpp"
#include "Win32Exception.hpp"
#include "Dx12/DxResource.hpp"
#include <stdexcept>

LRESULT CALLBACK WndProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam) {
	switch (Msg) {
		case WM_CLOSE:
			DestroyWindow(hWnd);
			return 0;
		case WM_DESTROY:
			PostQuitMessage(0);
			return 0;
		default:
			break;
	}
	return DefWindowProc(hWnd, Msg, wParam, lParam);
}

inline D3D12_GRAPHICS_PIPELINE_STATE_DESC CreatePipelineStateDesc(const Direct3D12::RootSignature& rootSignature) {
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc{};
	psoDesc.pRootSignature = rootSignature.Get();
	psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState.DepthEnable = FALSE;
	psoDesc.DepthStencilState.StencilEnable = FALSE;
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = BUFFERCOUNT;
	psoDesc.RTVFormats[0] = psoDesc.RTVFormats[1] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.SampleDesc.Count = 1;
	return psoDesc;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int CmdShow) {
	CoInitialize(nullptr);
	MSG msg{};
	try {
		MainWindow window = MainWindow(hInstance, WndProc, NULL, "Direct3D12", "#ffffff");
		window.CreateClientWindow(1280, 720);
		window.Show(CmdShow);
		window.Update();
		ZeroMemory(&msg, sizeof(msg));
		while (msg.message != WM_QUIT) {
			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
	}
	catch (const std::exception& er) {

	}
	catch (const Win32ExceptionA& awex) {

	}
	catch (const Win32ExceptionW& wwex) {

	}
	CoUninitialize();
	return static_cast<int>(msg.wParam);
}
