#ifndef __DXRESOURCE_HPP__
#define __DXRESOURCE_HPP__
#ifndef BUFFERCOUNT
#define BUFFERCOUNT 2u
#endif
#if (BUFFERCOUNT >= 1)
#include "Base.hpp"
#include "d3dx12.h"
#include "../HandleManager.hpp"
#include "../Window.hpp"
#include <dxgi1_4.h>
#include <vector>
#include <array>

namespace Direct3D12 {
	class Device;
	class CommandQueue;
}

namespace Dxgi {
	class Factory : public Direct3D12::Base<IDXGIFactory4> {
	public:
		Factory();
		Factory(std::nullptr_t) : InternalClassType() {}
	};

	class SwapChain : public Direct3D12::Base<IDXGISwapChain3> {
	private:
		unsigned int FrameIndex;
	public:
		SwapChain() : InternalClassType(nullptr) {};
		SwapChain(std::nullptr_t) : InternalClassType(nullptr) {}
		SwapChain(const Factory& factory, const Direct3D12::CommandQueue& CmdQueue, const HWND& WindowHandle, const unsigned int WindowWidth, const unsigned int WindowHeight);
		SwapChain(const Factory& factory, const Direct3D12::CommandQueue& CmdQueue, const HWND& WindowHandle, const DXGI_SWAP_CHAIN_DESC1& desc);
		template<typename T = unsigned int, std::enable_if_t<std::is_integral_v<T>, std::nullptr_t> = nullptr>
		T GetFrameIndex() const noexcept { return static_cast<T>(this->FrameIndex); }
		void UpdateFrameIndex();
	};
}

namespace Direct3D12 {
	class GPUData {
	public:
		GPUData() = default;
		GPUData(ComPtr<IDXGIAdapter1>&& Adapter, const DXGI_ADAPTER_DESC1& DescInfo, const ComPtr<ID3D12Device>& TempDev);
	private:
		ComPtr<IDXGIAdapter1> HardwareAdapter;
		D3D_FEATURE_LEVEL MaxSupportedFeatureLevel;
	public:
		std::basic_string<TCHAR> Name;
		unsigned int HardwareVendorId;
		unsigned int HardwareDeviceId;
		unsigned int HardwareSubSystemId;
		unsigned int HardwareRevision;
		size_t DedicatedVideoMemory;
		size_t DedicatedSystemMemory;
		size_t SharedSystemMemory;
		LUID AdapterLuid;
		unsigned int Flags;
		const ComPtr<IDXGIAdapter1>& GetAdapterData() const noexcept;
		const D3D_FEATURE_LEVEL& GetMaxSupportedFeatureLevel() const noexcept;
	};

	class GPUList : private std::vector<GPUData> {
	private:
		typedef std::vector<GPUData> InternalType;
		typedef InternalType::const_iterator citerator;
		typedef InternalType::const_reverse_iterator criterator;
	public:
		GPUList() = default;
		GPUList(const Dxgi::Factory& factory);
		citerator begin() const noexcept { return InternalType::begin(); }
		citerator end() const noexcept { return InternalType::end(); }
		citerator cbegin() const noexcept { return InternalType::cbegin(); }
		citerator cend() const noexcept { return InternalType::cend(); }
		criterator rbegin() const noexcept { return InternalType::rbegin(); }
		criterator rend() const noexcept { return InternalType::rend(); }
		criterator crbegin() const noexcept { return InternalType::crbegin(); }
		criterator crend() const noexcept { return InternalType::crend(); }
		const GPUData& at(const size_t pos) const { return InternalType::at(pos); }
		const GPUData& operator [] (const size_t pos) const { return InternalType::at(pos); }
		size_t size() const noexcept { return InternalType::size(); }
		bool empty() const noexcept { return InternalType::empty(); }
		const GPUData* data() const noexcept { return InternalType::data(); }
		const GPUData& front() const noexcept { return InternalType::front(); }
		const GPUData& back() const noexcept { return InternalType::back(); }
	};

	class Device : public Base<ID3D12Device> {
	private:
		D3D_FEATURE_LEVEL CurrentFeatureLevel;
	public:
		Device() : InternalClassType(nullptr) {};
		Device(std::nullptr_t) : InternalClassType(nullptr) {}
		// Warp Adapterを使用してデバイスを作成する
		Device(const Dxgi::Factory& factory);
		// 指定されたHardware Adapterを使用してデバイスを作成する
		Device(const GPUData& DataBuf);
	};

	class CommandQueue : public Base<ID3D12CommandQueue> {
	public:
		CommandQueue() : InternalClassType(nullptr) {};
		CommandQueue(std::nullptr_t) : InternalClassType(nullptr) {}
		CommandQueue(const Device& device);
		CommandQueue(const Device& device, const D3D12_COMMAND_QUEUE_DESC& desc);
	};

	namespace Impl {
		class DescriptorHeap : public Base<ID3D12DescriptorHeap> {
		protected:
			DescriptorHeap(std::nullptr_t) : InternalClassType(nullptr) {}
			DescriptorHeap(const Device& device, const D3D12_DESCRIPTOR_HEAP_DESC& desc);
		};
	}

	class RenderTargetViewDescriptorHeap : public Impl::DescriptorHeap {
	public:
		RenderTargetViewDescriptorHeap() : Impl::DescriptorHeap(nullptr) {};
		RenderTargetViewDescriptorHeap(std::nullptr_t) : Impl::DescriptorHeap(nullptr) {}
	private:
		RenderTargetViewDescriptorHeap(const Device& Dx12Device, const unsigned int BufferCount);
		RenderTargetViewDescriptorHeap(const Device& Dx12Device, const D3D12_DESCRIPTOR_HEAP_DESC& desc, const unsigned int BufferCount);
	public:
		RenderTargetViewDescriptorHeap(const Device& Dx12Device)
			: RenderTargetViewDescriptorHeap(Dx12Device, BUFFERCOUNT) {}
		RenderTargetViewDescriptorHeap(const Device& Dx12Device, const D3D12_DESCRIPTOR_HEAP_DESC& desc)
			: RenderTargetViewDescriptorHeap(Dx12Device, desc, BUFFERCOUNT) {}
		unsigned int rtvDescriptorSize;
	};

	class ShaderResourceViewDescriptorHeap : public Impl::DescriptorHeap {
	public:
		ShaderResourceViewDescriptorHeap() : Impl::DescriptorHeap(nullptr) {};
		ShaderResourceViewDescriptorHeap(std::nullptr_t) : Impl::DescriptorHeap(nullptr) {}
	private:
		ShaderResourceViewDescriptorHeap(const Device& Dx12Device, const unsigned int BufferCount);
		ShaderResourceViewDescriptorHeap(const Device& Dx12Device, const D3D12_DESCRIPTOR_HEAP_DESC& desc, const unsigned int BufferCount);
	public:
		ShaderResourceViewDescriptorHeap(const Device& Dx12Device)
			: ShaderResourceViewDescriptorHeap(Dx12Device, BUFFERCOUNT) {}
		ShaderResourceViewDescriptorHeap(const Device& Dx12Device, const D3D12_DESCRIPTOR_HEAP_DESC& desc)
			: ShaderResourceViewDescriptorHeap(Dx12Device, desc, BUFFERCOUNT) {}
	};

	namespace Impl {
		class RTV : public Base<ID3D12Resource> {
		public:
			RTV() : InternalClassType(nullptr) {};
			RTV(std::nullptr_t) : InternalClassType(nullptr) {}
			RTV(const Device& dev, const Dxgi::SwapChain& sw, const D3D12_RENDER_TARGET_VIEW_DESC& Dx12RTVDesc, CD3DX12_CPU_DESCRIPTOR_HANDLE& DescHandle, const unsigned int DescriptorSize, const size_t CurrentBufferCount);
		};
	}

	class RenderTargetView : private std::array<Impl::RTV, BUFFERCOUNT> {
	public:
		RenderTargetView() { for (int i = 0; i < BUFFERCOUNT; i++) std::array<Impl::RTV, BUFFERCOUNT>::at(i) = std::move(Impl::RTV(nullptr)); }
		RenderTargetView(const Device& Dx12Device, const Dxgi::SwapChain& DxgiSwapChain, const RenderTargetViewDescriptorHeap& Dx12RTVDescHeap);
	private:
		RenderTargetView(const Device& Dx12Device, const Dxgi::SwapChain& DxgiSwapChain, const RenderTargetViewDescriptorHeap& Dx12RTVDescHeap, const D3D12_RENDER_TARGET_VIEW_DESC& Dx12RTVDesc, const size_t BufferCount);
	public:
		RenderTargetView(const Device& Dx12Device, const Dxgi::SwapChain& DxgiSwapChain, const RenderTargetViewDescriptorHeap& Dx12RTVDescHeap, const D3D12_RENDER_TARGET_VIEW_DESC& Dx12RTVDesc) 
			: RenderTargetView(Dx12Device, DxgiSwapChain, Dx12RTVDescHeap, Dx12RTVDesc, BUFFERCOUNT) {}
		size_t GetBufferCount() const noexcept { return std::array<Impl::RTV, BUFFERCOUNT>::size(); }
		Base<ID3D12Resource>& at(const size_t pos) { return std::array<Impl::RTV, BUFFERCOUNT>::at(pos); }
		const Base<ID3D12Resource>& at(const size_t pos) const { return std::array<Impl::RTV, BUFFERCOUNT>::at(pos); }
		Base<ID3D12Resource>& operator [] (const size_t pos) { return std::array<Impl::RTV, BUFFERCOUNT>::operator[](pos); }
		const Base<ID3D12Resource>* data() const noexcept { return std::array<Impl::RTV, BUFFERCOUNT>::data(); }
		const Base<ID3D12Resource>& front() const noexcept { return std::array<Impl::RTV, BUFFERCOUNT>::front(); }
		const Base<ID3D12Resource>& back() const noexcept { return std::array<Impl::RTV, BUFFERCOUNT>::back(); }
	};

	class RootSignature : public Base<ID3D12RootSignature> {
	public:
		RootSignature() : InternalClassType(nullptr) {};
		RootSignature(std::nullptr_t) : InternalClassType(nullptr) {}
		RootSignature(const Device& Dx12Device, const unsigned int NodeMask = 0);
		RootSignature(const Device& Dx12Device, const D3D12_ROOT_SIGNATURE_DESC desc, const unsigned int NodeMask = 0);
	};

	class Shader : public Base<ID3DBlob> {
	public:
		Shader() : InternalClassType(nullptr) {};
		Shader(std::nullptr_t) : InternalClassType(nullptr) {}
		Shader(const std::string& ShaderFilePath, const std::string& EntryPoint, const std::string& Target);
	};

	class PipelineState : public Base<ID3D12PipelineState> {
	public:
		PipelineState() : InternalClassType(nullptr) {};
		PipelineState(std::nullptr_t) : InternalClassType(nullptr) {}
		PipelineState(const Device& Dx12Device, const RootSignature& Dx12RootSignature, const Shader& VertexShader, const Shader& PixelShader);
		PipelineState(const Device& Dx12Device, const D3D12_GRAPHICS_PIPELINE_STATE_DESC& desc);
	};

	class CommandAllocator : public Base<ID3D12CommandAllocator> {
	public:
		CommandAllocator() : InternalClassType(nullptr) {};
		CommandAllocator(std::nullptr_t) : InternalClassType(nullptr) {}
		CommandAllocator(const Device& Dx12Device);
	};

	class CommandList : public Base<ID3D12GraphicsCommandList> {
	public:
		CommandList() : InternalClassType(nullptr) {};
		CommandList(std::nullptr_t) : InternalClassType(nullptr) {}
		CommandList(const Device& Dx12Device, const CommandAllocator& CmdAlloc, const PipelineState& pipeline = nullptr, const unsigned int NodeMask = 0);
		void SetResourceBarrier(const ComPtr<ID3D12Resource>& Dx12RTV, const D3D12_RESOURCE_STATES& Before, const D3D12_RESOURCE_STATES& After) const;
		void SetResourceBarrier(const std::vector<D3D12_RESOURCE_BARRIER>& Barriers) const;
	};

	class VertexBuffer : public Base<ID3D12Resource> {
	private:
		D3D12_VERTEX_BUFFER_VIEW VertexBufferView;
	public:
		VertexBuffer() : InternalClassType(nullptr) {};
	private:
		VertexBuffer(const Device& Dx12Device, const unsigned int VertexBufferSize);
		void MapResource(unsigned char* pVertexDataBegin);
		void UnmapResource();
		void CreateVertexBufferView(const unsigned int VertexTypeSize, const unsigned int VertexBufferSize);
	public:
		template<typename Vertex>
		VertexBuffer(const Device& Dx12Device, const Vertex Vertices[])
			: VertexBuffer(ID3D12Device, sizeof(Vertices)) {
			unsigned char* pVertexDataBegin;
			this->MapResource(pVertexDataBegin);
			memcpy(pVertexDataBegin, Vertices, sizeof(Vertices));
			this->UnmapResource();
			this->CreateVertexBufferView(sizeof(Vertex), sizeof(Vertices));
		}
	};

	class IndexBuffer : public Base<ID3D12Resource> {
	private:
		D3D12_INDEX_BUFFER_VIEW IndexBufferView;
	public:
		IndexBuffer() : InternalClassType(nullptr) {};
	private:
		IndexBuffer(const Device& Dx12Device, const unsigned int IndexBufferSize);
		void MapResource(unsigned char* pIndexDataBegin);
		void UnmapResource();
		void CreateIndexBufferView(const unsigned int IndexBufferSize);
	public:
		template<typename T, std::enable_if_t<std::is_arithmetic_v<T>, std::nullptr_t> = nullptr>
		IndexBuffer(const Device& Dx12Device, const T IndexData[])
			: IndexBuffer(Dx12Device, sizeof(IndexData)) {
			unsigned char* pIndexDataBegin;
			this->MapResource(pIndexDataBegin);
			memcpy(pIndexDataBegin, IndexData, sizeof(IndexData));
			this->UnmapResource();
			this->CreateIndexBufferView(sizeof(IndexData));
		}
	};

	class Fence : public Base<ID3D12Fence> {
	private:
		HandleManager handle;
	public:
		Fence() : InternalClassType(nullptr) {};
		Fence(std::nullptr_t) : InternalClassType(nullptr) {}
		Fence(const Device& Dx12Device);
		unsigned __int64 FenceValue;
	};
}

class DxResource {
protected:
	Color ScreenClearColor;
	Dxgi::Factory DxgiFactory;
	Dxgi::SwapChain DxgiSwapChain;
	Direct3D12::Device Dx12Device;
	Direct3D12::CommandQueue Dx12CmdQueue;
	Direct3D12::RenderTargetViewDescriptorHeap Dx12DescHeap;
	Direct3D12::RenderTargetView Dx12RTV;
	Direct3D12::RootSignature Dx12RootSignature;
	Direct3D12::PipelineState Dx12PipelineState;
	Direct3D12::CommandAllocator Dx12CmdAlloc;
	Direct3D12::CommandList Dx12CmdList;
	Direct3D12::Fence Dx12Fence;
	CD3DX12_VIEWPORT DefViewport;
	CD3DX12_RECT DefScissorRect;
	virtual void Initialize(const MainWindow& Window);
public:
	DxResource(const MainWindow& Window, const Color& ScreenColorInfo);
	void Render();
	void Render(const std::vector<CD3DX12_VIEWPORT>& Viewport);
	void Render(const std::vector<CD3DX12_RECT>& ScissorRect);
	virtual void Render(const std::vector<CD3DX12_VIEWPORT>& Viewport, const std::vector<CD3DX12_RECT>& ScissorRect);
};
#endif
#endif
