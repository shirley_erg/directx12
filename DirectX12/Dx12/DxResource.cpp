#include "DxResource.hpp"
#include "Win32LetterConvert.hpp"
#include <d3dcompiler.h>
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d12.lib")
#pragma comment(lib, "d3dcompiler.lib")
#ifdef _DEBUG
#pragma comment(lib, "Win32LetterConvertDebug.lib")
constexpr unsigned int CompileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#else
#pragma comment(lib, "Win32LetterConvertRelease.lib")
constexpr unsigned int CompileFlags = 0;
#endif

namespace Dxgi {
#ifdef _DEBUG
	namespace {
		class DebugInterface : public Direct3D12::Base<ID3D12Debug> {
		public:
			DebugInterface()
				: InternalClassType(
					[](InternalType& res) { 
						return D3D12GetDebugInterface(IID_PPV_ARGS(&res));
					}
				) {}
		};
	}
#endif
	Factory::Factory()
		: InternalClassType(
			[](ComPtr<IDXGIFactory4>& res) {
				unsigned int Flag{};
#ifdef _DEBUG
				DebugInterface debug{};
				debug->EnableDebugLayer();
				Flag |= DXGI_CREATE_FACTORY_DEBUG;
#endif
			return CreateDXGIFactory1(IID_PPV_ARGS(&res));
			}
		) {}
	
	SwapChain::SwapChain(const Factory& factory, const Direct3D12::CommandQueue& CmdQueue, const HWND& WindowHandle, const unsigned int WindowWidth, const unsigned int WindowHeight)
		: SwapChain(factory, CmdQueue, WindowHandle, 
			[&WindowHandle, WindowWidth, WindowHeight]() {
				DXGI_SWAP_CHAIN_DESC1 desc = {};
				desc.BufferCount = BUFFERCOUNT;
				desc.Width = WindowWidth;
				desc.Height = WindowHeight;
				desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
				desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT | DXGI_USAGE_SHADER_INPUT;
				desc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
				desc.SampleDesc.Count = 1;
				desc.SampleDesc.Quality = 0;
				desc.Flags = 0;
				return desc;
			}()
		) {}

	SwapChain::SwapChain(const Factory& factory, const Direct3D12::CommandQueue& CmdQueue, const HWND& WindowHandle, const DXGI_SWAP_CHAIN_DESC1& desc)
		: InternalClassType(
			[&factory, &CmdQueue, &WindowHandle, &desc](InternalType& res) {
				ComPtr<IDXGISwapChain1> chain;
				if (const HRESULT hr = factory->CreateSwapChainForHwnd(CmdQueue.Get(), WindowHandle, &desc, nullptr, nullptr, &chain); FAILED(hr)) return hr;
				if (const HRESULT hr = factory->MakeWindowAssociation(WindowHandle, DXGI_MWA_NO_ALT_ENTER); FAILED(hr)) return hr;
				if (const HRESULT hr = chain->QueryInterface(__uuidof(IDXGISwapChain3), reinterpret_cast<void**>(res.GetAddressOf())); FAILED(hr)) return hr;
				return S_OK;
			}
		) {}
	void SwapChain::UpdateFrameIndex() {
		this->FrameIndex = this->Get()->GetCurrentBackBufferIndex();
	}
}

namespace Direct3D12 {
	namespace {
		std::basic_string<TCHAR> to_tchar(const wchar_t* str) {
#ifdef UNICODE
			return std::wstring(str);
#else
			return WStringToString(str);
#endif
		}
	}
	GPUData::GPUData(ComPtr<IDXGIAdapter1>&& Adapter, const DXGI_ADAPTER_DESC1& DescInfo, const ComPtr<ID3D12Device>& TempDev) 
		: HardwareAdapter(std::move(Adapter)), Name(to_tchar(DescInfo.Description)), HardwareVendorId(DescInfo.VendorId), HardwareDeviceId(DescInfo.DeviceId),
		HardwareSubSystemId(DescInfo.SubSysId), HardwareRevision(DescInfo.Revision), DedicatedVideoMemory(DescInfo.DedicatedVideoMemory),
		DedicatedSystemMemory(DescInfo.DedicatedSystemMemory), SharedSystemMemory(DescInfo.SharedSystemMemory), 
		AdapterLuid(DescInfo.AdapterLuid), Flags(DescInfo.Flags) {
		const D3D_FEATURE_LEVEL FeatureLevelList[] = {
			D3D_FEATURE_LEVEL_12_1,
			D3D_FEATURE_LEVEL_12_0,
			D3D_FEATURE_LEVEL_11_1,
			D3D_FEATURE_LEVEL_11_0
		};
		D3D12_FEATURE_DATA_FEATURE_LEVELS FeatureLevels = {
			_countof(FeatureLevelList),
			FeatureLevelList,
			D3D_FEATURE_LEVEL_11_0
		};
		if (const HRESULT hr = TempDev->CheckFeatureSupport(D3D12_FEATURE_FEATURE_LEVELS, &FeatureLevels, sizeof(FeatureLevels));
			FAILED(hr)) this->MaxSupportedFeatureLevel = D3D_FEATURE_LEVEL_11_0;
		else this->MaxSupportedFeatureLevel = FeatureLevels.MaxSupportedFeatureLevel;
	}

	const ComPtr<IDXGIAdapter1>& GPUData::GetAdapterData() const noexcept{
		return this->HardwareAdapter;
	}

	const D3D_FEATURE_LEVEL& GPUData::GetMaxSupportedFeatureLevel() const noexcept {
		return MaxSupportedFeatureLevel;
	}

	GPUList::GPUList(const Dxgi::Factory& factory) {
		ComPtr<IDXGIAdapter1> HardwareAdapter;
		for (unsigned int i = 0; DXGI_ERROR_NOT_FOUND != factory->EnumAdapters1(i, &HardwareAdapter); i++) {
			DXGI_ADAPTER_DESC1 desc{};
			if (const HRESULT hr = HardwareAdapter->GetDesc1(&desc); 
				FAILED(hr) || desc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) continue;
			ComPtr<ID3D12Device> TempDev;
			if (SUCCEEDED(D3D12CreateDevice(HardwareAdapter.Get(), D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&TempDev))))
				InternalType::emplace_back(std::move(HardwareAdapter), desc, TempDev);
		}
	}

	Device::Device(const Dxgi::Factory& factory)
		: InternalClassType(
			[&factory](InternalType& res) {
				ComPtr<IDXGIAdapter> WarpAdapter;
				if (const HRESULT hr = factory->EnumWarpAdapter(IID_PPV_ARGS(&WarpAdapter)); FAILED(hr)) return hr;
				return D3D12CreateDevice(WarpAdapter.Get(), D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&res));
			}
		){}

	Device::Device(const GPUData& DataBuf)
		: InternalClassType(
			[&DataBuf](InternalType& res) {
				return D3D12CreateDevice(DataBuf.GetAdapterData().Get(), DataBuf.GetMaxSupportedFeatureLevel(), IID_PPV_ARGS(&res)); 
			}
		), CurrentFeatureLevel(DataBuf.GetMaxSupportedFeatureLevel()) {}

	CommandQueue::CommandQueue(const Device& device) 
		: CommandQueue(device, 
			[]() {
				D3D12_COMMAND_QUEUE_DESC desc = {};
				desc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
				desc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
				return desc;
			}()
		) {}
	
	CommandQueue::CommandQueue(const Device& device, const D3D12_COMMAND_QUEUE_DESC& desc)
		: InternalClassType(
			[&device, &desc](InternalType& res) {
				return device->CreateCommandQueue(&desc, IID_PPV_ARGS(&res));
			}
	) {}
	
	ShaderResourceViewDescriptorHeap::ShaderResourceViewDescriptorHeap(const Device& Dx12Device, const unsigned int BufferCount)
		: DescriptorHeap(Dx12Device,
				[BufferCount]() {
			D3D12_DESCRIPTOR_HEAP_DESC Desc{};
			ZeroMemory(&Desc, sizeof(Desc));
			Desc.NumDescriptors = BufferCount;
			Desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
			Desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
			return Desc;
		}()
	) {}

	ShaderResourceViewDescriptorHeap::ShaderResourceViewDescriptorHeap(const Device& Dx12Device, const D3D12_DESCRIPTOR_HEAP_DESC& desc, const unsigned int BufferCount)
		: DescriptorHeap(Dx12Device, [&desc, BufferCount] {
			if (desc.NumDescriptors != BufferCount) throw std::runtime_error("Descriptor num is wrong.");
			return desc;
		}()
	) {}

	namespace Impl {
		DescriptorHeap::DescriptorHeap(const Device& device, const D3D12_DESCRIPTOR_HEAP_DESC& desc)
			: InternalClassType(
				[&device, &desc](InternalType& res) {
					return device->CreateDescriptorHeap(&desc, IID_PPV_ARGS(&res));
				}
			) {}
		RTV::RTV(const Device& dev, const Dxgi::SwapChain& sw, const D3D12_RENDER_TARGET_VIEW_DESC& Dx12RTVDesc, CD3DX12_CPU_DESCRIPTOR_HANDLE& DescHandle, const unsigned int DescriptorSize, const size_t CurrentBufferCount)
			: InternalClassType(
				[&sw, &dev, &Dx12RTVDesc, &DescHandle, DescriptorSize, CurrentBufferCount](InternalType& res) -> HRESULT {
				if (const HRESULT hr = sw->GetBuffer(CurrentBufferCount, IID_PPV_ARGS(&res)); FAILED(hr)) return hr;
				dev->CreateRenderTargetView(res.Get(), &Dx12RTVDesc, DescHandle);
				DescHandle.Offset(static_cast<unsigned int>(CurrentBufferCount), DescriptorSize);
				return S_OK;
			}
		) {}
	}

	RenderTargetViewDescriptorHeap::RenderTargetViewDescriptorHeap(const Device& Dx12Device, const unsigned int BufferCount)
		: DescriptorHeap(Dx12Device,
			[BufferCount]() {
				D3D12_DESCRIPTOR_HEAP_DESC Desc{};
				ZeroMemory(&Desc, sizeof(Desc));
				Desc.NumDescriptors = BufferCount;
				Desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
				Desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
				return Desc;
			}()
		) {}

	RenderTargetViewDescriptorHeap::RenderTargetViewDescriptorHeap(const Device& Dx12Device, const D3D12_DESCRIPTOR_HEAP_DESC& desc, const unsigned int BufferCount)
		: DescriptorHeap(Dx12Device, [&desc, BufferCount] {
				if (desc.NumDescriptors != BufferCount) throw std::runtime_error("Descriptor num is wrong.");
				return desc;
			}()
		) {
		this->rtvDescriptorSize = Dx12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	}

	RenderTargetView::RenderTargetView(const Device& Dx12Device, const Dxgi::SwapChain& DxgiSwapChain, const RenderTargetViewDescriptorHeap& Dx12RTVDescHeap)
		: RenderTargetView(Dx12Device, DxgiSwapChain, Dx12RTVDescHeap, []() {
			D3D12_RENDER_TARGET_VIEW_DESC Desc;
			Desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
			Desc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;
			Desc.Texture2D.MipSlice = 0;
			Desc.Texture2D.PlaneSlice = 0;
			return Desc;
		}()
	) {}
	
	RenderTargetView::RenderTargetView(const Device& Dx12Device, const Dxgi::SwapChain& DxgiSwapChain, const RenderTargetViewDescriptorHeap& Dx12RTVDescHeap, const D3D12_RENDER_TARGET_VIEW_DESC& Dx12RTVDesc, const size_t BufferCount) {
		CD3DX12_CPU_DESCRIPTOR_HANDLE handle(Dx12RTVDescHeap->GetCPUDescriptorHandleForHeapStart());
		const unsigned int DescriptorSize = Dx12Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
		for (size_t i = 0; i < BufferCount; i++) std::array<Impl::RTV, BUFFERCOUNT>::at(i) = std::move(Impl::RTV(Dx12Device, DxgiSwapChain, Dx12RTVDesc, handle, DescriptorSize, i));
	}

	RootSignature::RootSignature(const Device& Dx12Device, const unsigned int NodeMask)
		: RootSignature(Dx12Device, 
			[]() {
				D3D12_ROOT_SIGNATURE_DESC desc = {};
				desc.NumParameters = 0;
				desc.pParameters = nullptr;
				desc.NumStaticSamplers = 0;
				desc.pStaticSamplers = nullptr;
				desc.Flags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;
				return desc;
		}(), NodeMask) {}

	RootSignature::RootSignature(const Device& Dx12Device, const D3D12_ROOT_SIGNATURE_DESC desc, const unsigned int NodeMask)
		: InternalClassType(
			[&Dx12Device, &desc, NodeMask](InternalType& res) {
				ComPtr<ID3DBlob> Signature, Error;
				if (const HRESULT hr = D3D12SerializeRootSignature(&desc, D3D_ROOT_SIGNATURE_VERSION_1, &Signature, &Error); FAILED(hr)) return hr;
				return Dx12Device->CreateRootSignature(NodeMask, Signature->GetBufferPointer(), Signature->GetBufferSize(), IID_PPV_ARGS(&res));
			}
		) {}

	Shader::Shader(const std::string& ShaderFilePath, const std::string& EntryPoint, const std::string& Target) 
		: InternalClassType(
			[&ShaderFilePath, &EntryPoint, &Target] (InternalType& res) { return D3DCompileFromFile(StringToWString(ShaderFilePath).c_str(), nullptr, nullptr, EntryPoint.c_str(), Target.c_str(), CompileFlags, 0, &res, nullptr);	}
		) {}

	inline D3D12_GRAPHICS_PIPELINE_STATE_DESC CreateDefaultPipelineStateDesc(const RootSignature& Dx12RootSignature, const Shader& VertexShader, const Shader& PixelShader) {
		D3D12_GRAPHICS_PIPELINE_STATE_DESC desc{};
		D3D12_INPUT_ELEMENT_DESC InputElementDescs[] = {
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0,  0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
			{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
			{ "NORMAL",   0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT,    0, 24, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
		};
		desc.InputLayout = { InputElementDescs, _countof(InputElementDescs) };
		desc.pRootSignature = Dx12RootSignature.Get();
		auto shaderByteCode = [](ID3DBlob* shader) {
			D3D12_SHADER_BYTECODE code;
			code.pShaderBytecode = shader->GetBufferPointer();
			code.BytecodeLength = shader->GetBufferSize();
			return code;
		};
		desc.VS = shaderByteCode(VertexShader.Get());
		desc.PS = shaderByteCode(PixelShader.Get());
		desc.RasterizerState = []() {
			D3D12_RASTERIZER_DESC desc;
			desc.FillMode = D3D12_FILL_MODE_SOLID;
			desc.CullMode = D3D12_CULL_MODE_BACK;
			desc.FrontCounterClockwise = FALSE;
			desc.DepthBias = D3D12_DEFAULT_DEPTH_BIAS;
			desc.DepthBiasClamp = D3D12_DEFAULT_DEPTH_BIAS_CLAMP;
			desc.SlopeScaledDepthBias = D3D12_DEFAULT_SLOPE_SCALED_DEPTH_BIAS;
			desc.DepthClipEnable = TRUE;
			desc.MultisampleEnable = FALSE;
			desc.AntialiasedLineEnable = FALSE;
			desc.ForcedSampleCount = 0;
			desc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;
			return desc;
		}();
		desc.BlendState = []() {
			D3D12_BLEND_DESC desc = {};
			desc.AlphaToCoverageEnable = FALSE;
			desc.IndependentBlendEnable = FALSE;
			for (unsigned int i = 0; i < D3D12_SIMULTANEOUS_RENDER_TARGET_COUNT; i++)
				desc.RenderTarget[i] = []() {
				D3D12_RENDER_TARGET_BLEND_DESC desc;
				desc.BlendEnable = FALSE;
				desc.LogicOpEnable = FALSE;
				desc.SrcBlend = D3D12_BLEND_ONE;
				desc.DestBlend = D3D12_BLEND_ZERO;
				desc.BlendOp = D3D12_BLEND_OP_ADD;
				desc.SrcBlendAlpha = D3D12_BLEND_ONE;
				desc.DestBlendAlpha = D3D12_BLEND_ZERO;
				desc.BlendOpAlpha = D3D12_BLEND_OP_ADD;
				desc.LogicOp = D3D12_LOGIC_OP_NOOP;
				desc.RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
				return desc;
			}();
			return desc;
		}();
		desc.DepthStencilState.DepthEnable = FALSE;
		desc.DepthStencilState.StencilEnable = FALSE;
		desc.SampleMask = UINT_MAX;
		desc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
		desc.NumRenderTargets = 1;
		desc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
		desc.SampleDesc.Count = 1;
		return desc;
	}

	PipelineState::PipelineState(const Device& Dx12Device, const RootSignature& Dx12RootSignature, const Shader& VertexShader, const Shader& PixelShader)
		: PipelineState(Dx12Device, CreateDefaultPipelineStateDesc(Dx12RootSignature, VertexShader, PixelShader)) {}

	PipelineState::PipelineState(const Device& Dx12Device, const D3D12_GRAPHICS_PIPELINE_STATE_DESC& desc)
		: InternalClassType([&Dx12Device, &desc](InternalType& res) { return Dx12Device->CreateGraphicsPipelineState(&desc, IID_PPV_ARGS(&res)); }) {}

	CommandAllocator::CommandAllocator(const Device& Dx12Device)
		: InternalClassType([&Dx12Device](InternalType& res) { return Dx12Device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&res)); }) {}

	CommandList::CommandList(const Device& Dx12Device, const CommandAllocator& CmdAlloc, const PipelineState& pipeline, const unsigned int NodeMask)
		: InternalClassType([&Dx12Device, &CmdAlloc, &pipeline, NodeMask](InternalType& res) { return Dx12Device->CreateCommandList(NodeMask, D3D12_COMMAND_LIST_TYPE_DIRECT, CmdAlloc.Get(), pipeline.Get(), IID_PPV_ARGS(&res)); }) {
		this->Get()->Close();
	}

	void CommandList::SetResourceBarrier(const ComPtr<ID3D12Resource>& Dx12RTV, const D3D12_RESOURCE_STATES& Before, const D3D12_RESOURCE_STATES& After) const {
		D3D12_RESOURCE_BARRIER Barrier = {};
		Barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
		Barrier.Transition.pResource = Dx12RTV.Get();
		Barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
		Barrier.Transition.StateBefore = Before;
		Barrier.Transition.StateAfter = After;
		this->SetResourceBarrier({ Barrier });
	}

	void CommandList::SetResourceBarrier(const std::vector<D3D12_RESOURCE_BARRIER>& Barriers) const {
		this->Get()->ResourceBarrier(static_cast<unsigned int>(Barriers.size()), Barriers.data());
	}

	VertexBuffer::VertexBuffer(const Device& Dx12Device, const unsigned int VertexBufferSize)
		: InternalClassType([&Dx12Device, VertexBufferSize](InternalType& res) { 
			CD3DX12_HEAP_PROPERTIES HeapProperties(D3D12_HEAP_TYPE_UPLOAD);
			CD3DX12_RESOURCE_DESC desc = CD3DX12_RESOURCE_DESC::Buffer(VertexBufferSize);
			return Dx12Device->CreateCommittedResource(&HeapProperties, D3D12_HEAP_FLAG_NONE, &desc, D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, IID_PPV_ARGS(&res));
		}) {}

	void VertexBuffer::MapResource(unsigned char* pVertexDataBegin) {
		CD3DX12_RANGE ReadRange(0, 0);
		ThrowExceptionIfFailed(
			this->Get()->Map(0, &ReadRange, reinterpret_cast<void**>(pVertexDataBegin)),
			"Failed to map vertex data"
		);
	}

	void VertexBuffer::UnmapResource() {
		this->Get()->Unmap(0, nullptr);
	}

	void VertexBuffer::CreateVertexBufferView(const unsigned int VertexTypeSize, const unsigned int VertexBufferSize) {
		this->VertexBufferView.BufferLocation = this->Get()->GetGPUVirtualAddress();
		this->VertexBufferView.StrideInBytes = VertexTypeSize;
		this->VertexBufferView.SizeInBytes = VertexBufferSize;
	}

	IndexBuffer::IndexBuffer(const Device& Dx12Device, const unsigned int VertexBufferSize)
		: InternalClassType([&Dx12Device, VertexBufferSize](InternalType& res) {
		CD3DX12_HEAP_PROPERTIES HeapProperties(D3D12_HEAP_TYPE_UPLOAD);
		CD3DX12_RESOURCE_DESC desc = CD3DX12_RESOURCE_DESC::Buffer(VertexBufferSize);
		return Dx12Device->CreateCommittedResource(&HeapProperties, D3D12_HEAP_FLAG_NONE, &desc, D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, IID_PPV_ARGS(&res));
	}) {}

	void IndexBuffer::MapResource(unsigned char* pVertexDataBegin) {
		CD3DX12_RANGE ReadRange(0, 0);
		ThrowExceptionIfFailed(
			this->Get()->Map(0, &ReadRange, reinterpret_cast<void**>(pVertexDataBegin)),
			"Failed to map vertex data"
		);
	}

	void IndexBuffer::UnmapResource() {
		this->Get()->Unmap(0, nullptr);
	}

	void IndexBuffer::CreateIndexBufferView(const unsigned int IndexBufferSize) {
		this->IndexBufferView.BufferLocation = this->Get()->GetGPUVirtualAddress();
		this->IndexBufferView.Format = DXGI_FORMAT_R16_UINT;
		this->IndexBufferView.SizeInBytes = IndexBufferSize;
	}

	Fence::Fence(const Device& Dx12Device)
		: InternalClassType([&Dx12Device](InternalType& res) {
			return Dx12Device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&res));
		}
	) {
		this->FenceValue = 1;
	}
}

inline D3D12_GRAPHICS_PIPELINE_STATE_DESC CreatePipelineStateDesc(const Direct3D12::RootSignature& rootSignature) {
	D3D12_GRAPHICS_PIPELINE_STATE_DESC psoDesc{};
	psoDesc.pRootSignature = rootSignature.Get();
	psoDesc.RasterizerState = CD3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
	psoDesc.BlendState = CD3DX12_BLEND_DESC(D3D12_DEFAULT);
	psoDesc.DepthStencilState.DepthEnable = FALSE;
	psoDesc.DepthStencilState.StencilEnable = FALSE;
	psoDesc.SampleMask = UINT_MAX;
	psoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	psoDesc.NumRenderTargets = BUFFERCOUNT;
	psoDesc.RTVFormats[0] = psoDesc.RTVFormats[1] = DXGI_FORMAT_R8G8B8A8_UNORM;
	psoDesc.SampleDesc.Count = 1;
	return psoDesc;
}

DxResource::DxResource(const MainWindow& Window, const Color& ScreenColorInfo) 
	: ScreenClearColor(ScreenColorInfo) {
	this->Initialize(Window);
}

void DxResource::Initialize(const MainWindow& Window) {
	this->DxgiFactory = Dxgi::Factory();
	this->Dx12Device = Direct3D12::Device(this->DxgiFactory);
	this->Dx12CmdQueue = Direct3D12::CommandQueue(this->Dx12Device);
	this->DxgiSwapChain = Dxgi::SwapChain(this->DxgiFactory, this->Dx12CmdQueue, Window.hWnd, Window.WindowWidth, Window.WindowHeight);
	this->Dx12DescHeap = Direct3D12::RenderTargetViewDescriptorHeap(this->Dx12Device);
	this->Dx12RTV = Direct3D12::RenderTargetView(this->Dx12Device, this->DxgiSwapChain, this->Dx12DescHeap);
	this->Dx12RootSignature = Direct3D12::RootSignature(this->Dx12Device);
	const auto psoDesc = CreatePipelineStateDesc(this->Dx12RootSignature);
	this->Dx12PipelineState = Direct3D12::PipelineState(this->Dx12Device, psoDesc);
	this->Dx12CmdAlloc = Direct3D12::CommandAllocator(this->Dx12Device);
	this->Dx12CmdList = Direct3D12::CommandList(this->Dx12Device, this->Dx12CmdAlloc);
	this->Dx12Fence = Direct3D12::Fence(this->Dx12Device);
	this->DefViewport = CD3DX12_VIEWPORT(0.0f, 0.0f, static_cast<float>(Window.WindowWidth), static_cast<float>(Window.WindowHeight));
	this->DefScissorRect = CD3DX12_RECT(0, 0, Window.WindowWidth, Window.WindowHeight);
}

void DxResource::Render() {
	this->Render({ this->DefViewport }, { this->DefScissorRect });
}

void DxResource::Render(const std::vector<CD3DX12_VIEWPORT>& Viewport) {
	this->Render(Viewport, { this->DefScissorRect });
}

void DxResource::Render(const std::vector<CD3DX12_RECT>& ScissorRect) {
	this->Render({ this->DefViewport }, ScissorRect);
}


void DxResource::Render(const std::vector<CD3DX12_VIEWPORT>& Viewport, const std::vector<CD3DX12_RECT>& ScissorRect) {
	this->Dx12CmdAlloc->Reset();
	this->Dx12CmdList->Reset(this->Dx12CmdAlloc.Get(), this->Dx12PipelineState.Get());
	this->Dx12CmdList->SetGraphicsRootSignature(this->Dx12RootSignature.Get());
	this->Dx12CmdList->RSSetViewports(static_cast<unsigned int>(Viewport.size()), Viewport.data());
	this->Dx12CmdList->RSSetScissorRects(static_cast<unsigned int>(ScissorRect.size()), ScissorRect.data());
	this->Dx12CmdList.SetResourceBarrier(this->Dx12RTV[this->DxgiSwapChain.GetFrameIndex()].Get(), D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET);
	CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(this->Dx12DescHeap->GetCPUDescriptorHandleForHeapStart(), this->DxgiSwapChain.GetFrameIndex<int>(), this->Dx12DescHeap.rtvDescriptorSize);
	this->Dx12CmdList->OMSetRenderTargets(1, &rtvHandle, FALSE, nullptr);
	this->Dx12CmdList->ClearRenderTargetView(rtvHandle, this->ScreenClearColor[1.0].data(), static_cast<unsigned int>(ScissorRect.size()), ScissorRect.data());
	this->Dx12CmdList.SetResourceBarrier(this->Dx12RTV[this->DxgiSwapChain.GetFrameIndex()].Get(), D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT);
	this->Dx12CmdList->Close();
	ID3D12CommandList* ppCommandLists[] = { this->Dx12CmdList.Get() };
	this->Dx12CmdQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);
	this->DxgiSwapChain->Present(1, 0);
	this->DxgiSwapChain.UpdateFrameIndex();
	this->Dx12CmdQueue->Signal(this->Dx12Fence.Get(), this->Dx12Fence.FenceValue);
	while (this->Dx12Fence->GetCompletedValue() < this->Dx12Fence.FenceValue);
	this->Dx12Fence.FenceValue++;
}
