#ifndef __BASE_HPP__
#define __BASE_HPP__
#include "ThrowExceptionIfFailed.hpp"
#include <wrl/client.h>
#include <functional>
#include <typeinfo>
#include <string>
template<typename C>
using ComPtr = Microsoft::WRL::ComPtr<C>;

namespace Direct3D12 {
	template<typename T>
	class Base {
	private:
		ComPtr<T> Resource;
	protected:
		typedef Base<T> InternalClassType;
		typedef ComPtr<T> InternalType;
	public:
		Base() : Resource(nullptr) {}
		Base(std::function<HRESULT(InternalType&)> InitFunction) {
			ThrowExceptionIfFailed(InitFunction(this->Resource),
				std::string("error in ")
				+ typeid(T).name()
				+ " initialize."
			);
		}
		Base(std::nullptr_t) : Resource(nullptr) {}
		T* Get() const { return this->Resource.Get(); }
		const T** GetAddressOf() const { return this->Resource.GetAddressOf(); }
		T** GetAddressOf() { return this->Resource.GetAddressOf(); }
		T* operator -> () const { return this->Resource.operator->(); }
		const InternalType& GetComPtr() const noexcept { return this->Resource; }
		bool operator == (const std::nullptr_t) const noexcept { return this->Resource == nullptr; }
		bool operator != (const std::nullptr_t) const noexcept { return this->Resource != nullptr; }
	};
}
#endif
