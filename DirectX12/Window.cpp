#include "Window.hpp"
#include "Win32Exception.hpp"
#ifdef _DEBUG
#pragma comment(lib, "Win32ExceptionDebug.lib")
#else
#pragma comment(lib, "Win32ExceptionRelease.lib")
#endif
namespace ColorCodeStartPoint {
	constexpr size_t Red = 1;
	constexpr size_t Green = 3;
	constexpr size_t Blue = 5;
}

namespace {
	inline int GetColorCodeToNum(const std::string str, const size_t Start) {
		return std::stoi(str.substr(Start, 2), nullptr, 16);
	}

	inline int GetColorCodeToNum(const std::wstring str, const size_t Start) {
		return std::stoi(str.substr(Start, 2), nullptr, 16);
	}

	inline bool IsColorCode(const std::string str) {
		return str.size() != 7 || '#' != str.at(0);
	}

	inline bool IsColorCode(const std::wstring str) {
		return str.size() != 7 || L'#' != str.at(0);
	}

	inline void ThrowExceptionIfNotColorCode(const std::string str) {
		if (!IsColorCode(str)) throw std::runtime_error("Color code is wrong.");
	}

	inline void ThrowExceptionIfNotColorCode(const std::wstring str) {
		if (!IsColorCode(str)) throw std::runtime_error("Color code is wrong.");
	}
}

Color::Color(const std::string ColorCode) {
	ThrowExceptionIfNotColorCode(ColorCode);
	Color(
		GetColorCodeToNum(ColorCode, ColorCodeStartPoint::Red),
		GetColorCodeToNum(ColorCode, ColorCodeStartPoint::Green),
		GetColorCodeToNum(ColorCode, ColorCodeStartPoint::Blue)
		);
}

Color::Color(const std::wstring ColorCode) {
	ThrowExceptionIfNotColorCode(ColorCode);
	Color(
		GetColorCodeToNum(ColorCode, ColorCodeStartPoint::Red),
		GetColorCodeToNum(ColorCode, ColorCodeStartPoint::Green),
		GetColorCodeToNum(ColorCode, ColorCodeStartPoint::Blue)
	);
}

HBRUSH Color::GetColor() const noexcept {
	return CreateSolidBrush(RGB(this->Red, this->Green, this->Blue));
}

std::array<float, 4> Color::operator [] (const float Alpha) const noexcept {
	return { static_cast<float>(this->Red) / 255, static_cast<float>(this->Green) / 255, static_cast<float>(this->Blue) / 255, Alpha };
}
HICON ImageIcon::GetImage(HINSTANCE InstanceHandle, const unsigned int LoadOption, const bool Cursor) const noexcept {
	return this->empty() ? NULL :
		(HICON)LoadImageA(InstanceHandle, this->FilePath.c_str(), Cursor ? IMAGE_CURSOR : IMAGE_ICON, this->IconWidth, this->IconHeight, LoadOption);
}

MainWindow::MainWindow(HINSTANCE hInstance, WNDPROC WndProc, LPCSTR MenuName, const std::string WindowTitle, const std::string BackgroundColorCode)
	: MainWindow(hInstance, WndProc, MenuName, WindowTitle, BackgroundColorCode, LoadIcon(NULL, IDC_ICON), LoadCursor(NULL, IDC_ARROW)) {}

MainWindow::MainWindow(HINSTANCE hInstance, WNDPROC WndProc, LPCSTR MenuName, const std::string WindowTitle, const std::string BackgroundColorCode,
	const ImageIcon Icon, const ImageIcon Cursor)
	: MainWindow(hInstance, WndProc, MenuName, WindowTitle, BackgroundColorCode, Icon.GetImage(hInstance, LR_SHARED), Cursor.GetImage(hInstance, LR_SHARED, true)) {}


MainWindow::MainWindow(HINSTANCE hInstance, WNDPROC WndProc, LPCSTR MenuName, const std::string WindowTitle, const std::string BackgroundColorCode, const HICON Icon, const HICON Cursor)
	: wc(), WindowTitle(WindowTitle), InstanceHandle(hInstance), hWnd() {
	this->wc.cbSize = sizeof(this->wc);
	this->wc.style = CS_HREDRAW | CS_VREDRAW;
	this->wc.lpfnWndProc = WndProc;
	this->wc.cbClsExtra = 0;
	this->wc.cbWndExtra = 0;
	this->wc.hInstance = hInstance;
	this->wc.hbrBackground = Color(BackgroundColorCode).GetColor();
	this->wc.lpszMenuName = MenuName;
	this->wc.lpszClassName = "DX12Window";
	this->wc.hIcon = this->wc.hIconSm = Icon;
	this->wc.hCursor = Cursor;
}

void MainWindow::CreateClientWindow(const int Width, const int Height) {
	if (0 == RegisterClassExA(&this->wc)) throw Win32ExceptionA();
	this->WindowWidth = Width;
	this->WindowHeight = Height;
	this->hWnd = CreateWindowExA(
		WS_EX_COMPOSITED,
		"DX12Test",
		this->WindowTitle.c_str(),
		WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX,
		CW_USEDEFAULT, CW_USEDEFAULT,
		WindowWidth, WindowHeight, NULL, NULL, this->InstanceHandle, NULL
	);
	if (this->hWnd == NULL) throw Win32ExceptionA();
}

void MainWindow::Show(const int CmdShow) {
	ShowWindow(this->hWnd, CmdShow);
}

void MainWindow::Update() {
	UpdateWindow(this->hWnd);
}
